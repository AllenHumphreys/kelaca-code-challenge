<?php
/**
 * Kelaca code challenge function library
 *
 * Library includes functions for calculation the mean, median, mode, and range
 * of a set of numbers
 *
 * @author     Allen Humphreys <allen.humphreys@icloud.com>
 */


/**
 * Calculates the mean
 *
 * @param array $arg_1  the array containing a set of numbers
 *
 * @return float  the floating point mean, rounded to three decimal places.
 *                NULL on incorrect input.
 *               
 */
function mean($arg_1)
{   
    if (is_array($arg_1) && $arg_1 != NULL && count($arg_1) > 0){
        return round(array_sum($arg_1)/count($arg_1),3);
    } else {
        return NULL;
    }
}

/**
 * Calculates the median
 *
 * @param array $arg_1  the array containing a set of numbers
 *
 * @return float  the floating point median, rounded to three decimal places.
 *                NULL on incorrect input.
 *               
 */
function median($arg_1)
{
    if (is_array($arg_1) && $arg_1 != NULL && count($arg_1) > 0){
        sort($arg_1);
        
        if (count($arg_1)%2 == 0){
            //if there are an even number of elements,
            //the median is the average of the two middle elements
            return round(($arg_1[round(count($arg_1)/2)] + $arg_1[round(count($arg_1)/2)-1])/2, 3);
        } else {
            //if there are an odd number elements,
            //just return the middle element
            return round($arg_1[round(count($arg_1)/2)-1],3);
        }
    } else {
        return NULL;
    }
}

/**
 * Calculates the mode
 *
 * @param array $arg_1  the array containing a set of numbers
 *
 * @return float  the floating point mode, rounded to three decimal places.
 *                NULL on incorrect input.
 *                an array containing all the modes of a multimodal set.
 *               
 */
function mode($arg_1){
    if (is_array($arg_1) && $arg_1 != NULL && count($arg_1) > 0){
        $frequency = array_count_values($arg_1);
        
        $most_occurences = 0;
        foreach ($frequency as $number=>$occurences){
            if ($occurences > $most_occurences){
                $most_occurences = $occurences; //reset the comparison point
                unset($mode);
                $mode = array($number);
            } else if ($occurences == $most_occurences){
                array_push($mode, $number);
            }
        }
        
        if (count($mode == 1)){
            return $mode[0];
        } else {
            return $mode;
        }
        
    } else {
        return NULL;
    }

}

/**
 * Calculates the range
 *
 * @param array $arg_1  the array containing a set of numbers
 *
 * @return float  the floating point range, rounded to three decimal places.
 *                NULL on incorrect input.
 *               
 */
function my_range($arg_1){
    if (is_array($arg_1) && $arg_1 != NULL && count($arg_1) > 0){
        sort($arg_1, SORT_NUMERIC);
        $min = $arg_1[0];
        $max = $arg_1[count($arg_1)-1];
        return $max - $min;
    } else {
        return NULL;
    }

}

?>