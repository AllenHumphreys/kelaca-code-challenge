<?php
/**
 * API gateway for CodeChallenge
 *
 * Expects POST request with POST data formated as JSON object including a
 * sequence of numbers to be operated on
 *
 * @author     Allen Humphreys <allen.humphreys@icloud.com>
 */

require_once '../lib/CodeChallengeLibrary.php';

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $json = json_decode($_POST['json_input'],TRUE);
    if (array_key_exists("numbers", $json)){
    $r_mean = mean($json["numbers"]);
    $r_median = median($json["numbers"]);
    $r_mode = mode($json["numbers"]);
    $r_range = my_range($json["numbers"]);
    
    echo json_encode(array("results" =>
                           array('mean' => $r_mean, 'median' => $r_median,
                                 'mode' => $r_mode, 'range' => $r_range)));
    } else{
        echo json_encode(array("error" =>
                       array("code" => 500,
                             "message" => "Unprocessable data")));
    }
    

}elseif ($_SERVER['REQUEST_METHOD'] == 'GET'){

echo json_encode(array("error" =>
                       array("code" => 405,
                             "message" => "Method GET not available on this endpoint")));
}


?>